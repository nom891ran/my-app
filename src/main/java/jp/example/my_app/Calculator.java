package jp.example.my_app;

import java.math.BigDecimal;

public class Calculator {

    public static int calc(int price, int count) throws Exception {
    	if(count >= 1) {
        return price * count;
    	}
    	return 0;
    }

    public static int withTax(int price) throws Exception {
        return new BigDecimal(price).multiply(
                new BigDecimal("1.08")).setScale(0, BigDecimal.ROUND_DOWN)
                .intValue();
    }

}
