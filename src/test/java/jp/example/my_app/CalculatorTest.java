package jp.example.my_app;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorTest {

	@Test
    public void test_calc() throws Exception {
        // given
        int price = 100;
        int count = 27;
        int expected = 2700;

        // when
        int actual = Calculator.calc(price, count);

        // then
        assertThat(actual, is(expected));
    }
	
	@Test
    public void test_calcZero() throws Exception {
        // given
        int price = 100;
        int count = -6;
        int expected = 0;

        // when
        int actual = Calculator.calc(price, count);

        // then
        assertThat(actual, is(expected));
    }
	
	@Test
	public void test_withTax() throws Exception {
		int price = 100;
		int expected = 108;
		
		int actual = Calculator.withTax(price);
		
		assertThat(actual, is(expected));
	}


}
