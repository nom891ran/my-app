package jp.example.my_app;

public class Order {

    public static int order(int[][] values) {

        int total = 0;

        try {
            for (int[] value : values) {
                int price = value[0];
                int count = value[1];
                total += Calculator.calc(price, count);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return total;
    }

}
